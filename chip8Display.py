'''

CHIP-8 Screen Emulator written by netcat n4d4s

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

CopyLeft 2022

'''

import pygame
from pygame.locals import *

class Display:
    '''Display
    :Version: 1.0.0
    '''
    
    def __init__(self):
        '''
        Display __init__
            Init Display class
        '''
        self.XSize = 64 	# Number of original Chip-8 pixels on the X axis
        self.YSize = 32 	# Number of original Chip-8 pixels on the Y axis
        
        self.SXSize = 0 	# Number of pixels for the screen on the X axis (stands for Screen X Size)
        self.SYSize = 0 	# Number of pixels for the screen on the Y axis (stands for Screen Y Size)

        self.upscaling = 10 # Upscaling ratio
        
        self.pixels = [] 	# Map of initial screen pixels
        self.clear() 		# Initialize the 2D list of pixels by clearing the screen
        
        self.regs = [0]*16 				# Store the register values to compare them against latest values
        self.regsPos = [(0,0)]*16 		# Store the positions of register values on screen
        self.regsSizes = [(0,0)]*16 	# Store the sizes of register values on screen

        self.vars = [('', 0)]*5 	# Store the different special registers to compare them against latest values
        self.vars[0] = ('DT',0) 	# Delay Timer
        self.vars[1] = ('ST',0) 	# Sound Timer
        self.vars[2] = ('I',0) 		# I register
        self.vars[3] = ('PC',0) 	# Program Counter
        self.vars[4] = ('SP',0) 	# Stack Pointer
        self.varsPos = [(0,0)]*5 	# Store the positions of those values on screen
        self.varsSizes = [(0,0)]*5 	# Store the sizes of thos values on screen

        self.mem = {} 		# Store the memory values displayed on screen, to compare them against latest values
        self.memPos = {} 	# Store the position of displayed memory values
        self.memSizes = {} 	# Store the size of displayed memory values
        self.memY = 0 		# Starting Y position for the memory debugger because commands are displayed above it
        self.PCPos = () 	# Store the position of the indicator showing the current memory adress

        self.gameFg = (255, 255, 255)            # Defining the white color (almost all times is the FG color)
        self.gameBg = (0, 0, 0) 			        # Defining the black color (almost all times is the BG color)
        self.debugSquareColor = (255, 255, 0)   # cippotto debug square color. Default to yellow

        self.pause = (0,0,0,0) 	# Store the position and size of the PAUSE label
        self.mute = (0,0,0,0) 	# Store the position and size of the MUTE label
        
        #self.clock = pygame.time.Clock()                   # This is an ugly test
        
        #self.beep = pygame.mixer.Sound('Assets/beep.ogg')  # Load the beep sample
        self.screenTitle = ''                               # Screen title. This property is populated dynamically with cippotto title and selected ROM
        self.softwareIcon = ''                              # This will contain the path of the image file used by PyGame window
        self.pauseText = 'PAUSE'                            # The string that will be displayed when the game is paused
        self.muteText = 'MUTE'                              # The string that will be displayed when player mute the audio
        
    def __str__(self):
        '''__str__
        :Version: 1.0.0
    
        Used to debug the class by displaying each pixel value with its coordinates
        
        :return: Formatted text to display each pixel value with its coordinates
        :rtype: string
        '''
        result = ''	    														# String that will be returned
        for x in range(0, self.XSize): 											# For each X
            for y in range(0, self.YSize): 										# For each Y
                result += "({}, {}) = {}\n".format(x, y, self.pixels[x][y]) 	# We print the coordinates and the value of the X - Y pixel
        return result
        
    def preInitSound(self, frequency=400, channels=1):
        '''preInitSound
        :Version: 1.0.0
        
        The only way that I found to set the number of audio channels is to call 
        ``pygame.mixer.pre_init`` before ``pygame.init``, so with this method I can call 
        ``pygame.mixer.pre_init`` before ``pygame.init`` everywhere
        
        .. todo::
            - I should remove this method from this class
        
        :param frequency: Audio frequency
        :type frequency: int 
        :param channels: The number of audio channels
        :type channels: int 
        
        '''
        pygame.mixer.pre_init(frequency=frequency, channels=channels, allowedchanges=0)
        
    def pygameInit(self):
        '''pygameInit
        :Version: 1.0.0
        
        Initialize Pygame and load a font to display text for the debugger
        
        '''
        pygame.init()
        self.font = pygame.font.Font(pygame.font.get_default_font(), 14)
        pygame.display.set_icon(pygame.image.load(self.softwareIcon))
        pygame.display.set_caption(self.screenTitle)
        
    def setSoftwareColor(self,bg,fg,debugSquareColor):
        '''setSoftwareColor
        :Version: 1.0.0
        
        Sets ``self.gameFg`` ``self.gameBg`` and ``self.debugSquareColor``
        
        :param bg: A string that represents a HEX color value like ``#000000`` used to set game background color (``self.gameBg``). Original CHIP-8 background is black (``#000000``)
        :type bg: string
        :param fg: A string that represents a HEX color value like ``#000000`` used to set game foreground color (``self.gameFg``). Original CHIP-8 background is white (``#FFFFFF``)
        :type fg: string 
        :param debugSquareColor: A string that represents a HEX color value like ``#000000`` used to set the square (``self.debugSquareColor``) visible when debug is on
        :type debugSquareColor: string 
        '''
        self.gameFg = fg
        self.gameBg = bg
        self.debugSquareColor = debugSquareColor
        
    def setSoftwareIcon(self,iconPath):
        '''setSoftwareIcon
        :Version: 1.0.0
        
        Given a path sets ``self.softwareIcon``
        
        :param iconPath: Absolute or relative path of the emulator icon
        :type iconPath: string 
        
        '''
        self.softwareIcon = iconPath
        
    def setScreenTitle(self, titleStr = ''):
        '''setScreenTitle
        :Version: 1.0.0
        
        Given a string set ``self.screenTitle``
        
        :param titleStr: Emulator default window title
        :type titleStr: string 
        
        '''
        self.screenTitle = self.screenTitle + ' - ' + titleStr
        pygame.display.set_caption(self.screenTitle)
        
    def clear(self):
        '''clear
        :Version: 1.0.0
        
        Clears the game screen by resetting the 2D list of pixels to 0
        
        '''
        self.pixels = [[0]*self.YSize for i in range(self.XSize)] # A 2D list representing the pixels of the game screen with a format of pixels[X][Y]
        
    def destroyScreen(self):
        '''destroyScreen
        :Version: 1.0.0
        
        Quit PyGame
        
        '''
        pygame.quit()

    def setDebugMargin(self, videoDebug=False):
        '''setDebugMargin
        :Version: 1.0.0
        
        Set video debug margins to display or not opcodes and registry informations during the game
        
        :param videoDebug: If True the margins are shown, otherwise in the window is shown only the game
        :type videoDebug: boolean 
        '''
        print('DEBUG: chip8Display.py setDebugMargin() - Setting video debug margins')#DEBUG
        if videoDebug:
            self.XMargin = 200 	# Number of pixels of margin to the right of the game screen to display the debugger
            self.YMargin = 105 	# Number of pixels of maring on the bottom of the game screen to display the debugger
        else:
            self.XMargin = 0 	# Number of pixels of margin to the right of the game screen to display the debugger
            self.YMargin = 0 	# Number of pixels of maring on the bottom of the game screen to display the debugger
        self.startScreen()

    def resizeScreen(self, newX, newY):
        '''resizeScreen
        :Version: 1.0.0
        
        Calculates new upscaling values when the window is resized, and resets it
        
        :param newX: The new game window width without margins
        :type newX: int 
        :param newY: The new game window height without margins
        :type newY: int 
        
        '''
        print('DEBUG resizeScreen(): newX',newX,'newY',newY)#DEBUG
        newX -= self.XMargin 						# Calculate X and Y available pixels for the game screen. We do it by taking the total new window size, and removing the necessary margins for the debugger
        newY -= self.YMargin
        XUpscale = newX // self.XSize 				# Calculate the upscaling we can provide for both the X and Y axis based of the available game screen with the new window
        YUpscale = newY // self.YSize
        self.upscaling = min(XUpscale, YUpscale) 	# Selecting the minimum of the X and Y upscaling in order to not increase the size of the new window
        self.startScreen() 							# Reset the screen with the new upscaling
        
    def startScreen(self):
        '''startScreen
        :Version: 1.0.0
        
        Initialize the whold window with the game screen and provides upscaling because the window is resizable
        
        '''
        
        self.SXSize = (self.XSize * self.upscaling) + self.XMargin 	# Calculating the window size by upscaling the game screen and adding the X and Y margins
        self.SYSize = (self.YSize * self.upscaling) + self.YMargin
        
        print('DEBUG: startScreen()','self.SXSize',self.SXSize,'self.SYSize',self.SYSize)#DEBUG

        self.window = pygame.display.set_mode((self.SXSize, self.SYSize), RESIZABLE) 	# Initialize the window with Pygame, enables resizing
        self.window.fill(self.gameBg)
        
        # Draw 2 lines on the bottom and right of the game screen in white, to separate the game with the debugger
        # First point is bottom left of game screen
        # Second one is bottom right of game screen
        # Third one is top right of game screen
        pygame.draw.lines(self.window, self.gameFg, False, [(0,self.YSize*self.upscaling), (self.XSize*self.upscaling, self.YSize*self.upscaling), (self.XSize*self.upscaling, 0)])

        self.memY = self.initCommands() 	# Display the available commands and retrieve at which Y position it ended (to display memory debugger under it)
        self.initRegsAndVars() 				# Initialize the labels of all registers (VX and special registers) under the game screen
        self.refreshScreen() 				# Refresh the window with all our new displayed magic
        
    def refreshScreen(self):
        '''refreshScreen
        :Version: 1.0.0
        
        Refresh the game screen
        
        '''
        for x in range(0, self.XSize): 			# For each X
            for y in range(0, self.YSize):		# For each Y
                if(self.pixels[x][y] == 0): 	# If the pixel is 0 (black)
                    color = self.gameBg 			# The color chosen will be our defined black color
                else:
                    color = self.gameFg 			# Or if the pixel is not 0 (so 1 or any unintended value which souldn't happen) we chose our defined white color

                # We draw our upscaled pixel, which is just a square with a length equals to our upscaling value starting with a position taking into account the upscaling
                # We are using the previously defined color based on our if
                pygame.draw.rect(self.window, color, (self.upscaling*x, self.upscaling*y, self.upscaling, self.upscaling))

        pygame.display.flip() 	# Updates the screen
        
    def drawText(self, text, X, Y):
        '''drawText
        :Version: 1.0.0
        
        Draw a text at X and Y position, and returns the size it takes
        
        :param text: The text to render
        :type text: string 
        :param X: Text x position in the window
        :type X: int 
        :param Y: Text y position in the window
        :type Y: int 
        
        :return: Dimensions needed to render the text
        :rtype: tuple
        '''
        self.window.blit(self.font.render(text, True, self.gameFg), (X, Y)) 	# Renders the text and adds it to the window
        return self.font.size(text) 										# Returns the text size
        
    def initRegsAndVars(self):
        '''initRegsAndVars
        :Version: 1.0.0
        
        Displays the registers labels
        
        '''
        initialY = self.YSize*self.upscaling + 10 	# Calculate the initial Y value (right under the game screen)
        y = initialY 								# Saves the initial Y to the variable that will be used to move the drawing cursor
        x = 10 										# The cursor for the X axis
        xMargin = 50 								# Margin the X axis between labels

        maxX = 0 	# Maximum X size taken by labels on the row to have some sort of alignement and no label writing on each other

        for i in range(len(self.regs)): 	# For each index of V registers

            size = self.drawText("V{:X}=".format(i), x, y) 	# Display the label with its hexadecimal index at the current X and Y position
            maxX = max(maxX, size[0]) 						# Compare the X size to the maximum

            self.regsPos[i] = (x+size[0], y) 	# Gets the X and Y values where the register value will be displayed. The X value is the current position + the label size

            self.regsSizes[i] = self.drawText("#{:0=4X}".format(self.regs[i]), self.regsPos[i][0], self.regsPos[i][1]) 	# Display the current register value next to its label with the previoulsy calculated coordinates

            if(y + 2*size[1] < self.SYSize): 	# If the next register can still be displayed under our current one. We add twice our label height because we take into account the current one (our cursor is still above it), and the next one.
                y += size[1] 					# We then move our Y cursor under our current lable
            else: 								# The next register label can't be displayed under the current one
                x += maxX + xMargin 			# We move our X cursor to the right by our defined margin (which takes the register value size into account) and the max X size of our labels in the column
                maxX = 0 						# We reset the max X size because we are switching to another column
                y = initialY 					# We reset the Y cursor position

        y = initialY 			# Once we have displayed the V registers, we reset the Y cursor poisition because special registers will be displayed on another column
        x += maxX + 2*xMargin 	# We move our X cursor position by the standard margin but with some more in order to differentiate those two types of registers
        maxX = 0 				# We reset the max X size

        for idx in range(len(self.vars)): 	# For each special register

            size = self.drawText("{} = ".format(self.vars[idx][0]), x, y) 	# Display the label at the current X and Y position
            maxX = max(maxX, size[0]) 										# Compare the X size to the maximum

            self.varsPos[idx] = (x+size[0], y) 	# Gets the X and Y values where the register value will be displayed. The X value is the current position + the label size

            self.varsSizes[idx] = self.drawText("#{:0=4X}".format(self.vars[idx][1]), self.varsPos[idx][0], self.varsPos[idx][1]) 	# Display the current register value next to its label with the previoulsy calculated coordinates

            if(y + 2*size[1] < self.SYSize): 	# Same idea as the above if statement, we check if we can write another register under our current one, if not we move it to the next column
                y += size[1]
            else:
                x += maxX + 50
                maxX = 0
                y = initialY

    def displayRegsAndVars(self, regs, DT, ST, I, PC, SP):
        '''displayRegsAndVars
        :Version: 1.0.0
        
        Displays the ``V`` and special registers in the game window
        
        :param regs: The list that contain ``V`` register values
        :type regs: chip8CPU._UnsignedBitsArray
        :param DT: Delay timer value
        :type DT: int
        :param ST: Sound Timer value
        :type ST: int
        :param I: I register value
        :type I: int
        :param PC: Program Counter value
        :type PC: int
        :param SP: Stack Pointer value
        :type SP: int
        
        '''
        for idx, val in enumerate(regs): 	# For each V register
            if(val != self.regs[idx]): 	# If the current CPU value is different than the one stored by the screen
                self.regs[idx] = val 	# We update the stored one
                pygame.draw.rect(self.window, self.gameBg, (self.regsPos[idx][0], self.regsPos[idx][1], self.regsSizes[idx][0], self.regsSizes[idx][1])) 	# We erase the displayed value with a black rectangle using the previously saved size and position
                self.regsSizes[idx] = self.drawText("#{:0=4X}".format(self.regs[idx]), self.regsPos[idx][0], self.regsPos[idx][1]) 	# We redraw the text on the screen and save its new size

        otherVars = [DT, ST, I, PC, SP] 	# We list all needed special registers values to ease the process with a for loop

        for idx, val in enumerate(otherVars): 	# For each special register, we do the exact same thing as the above for statement
            if(val != self.vars[idx][1]):
                self.vars[idx] = (self.vars[idx][0], val)
                pygame.draw.rect(self.window, self.gameBg, (self.varsPos[idx][0], self.varsPos[idx][1], self.varsSizes[idx][0], self.varsSizes[idx][1]))
                self.varsSizes[idx] = self.drawText("#{:0=4X}".format(self.vars[idx][1]), self.varsPos[idx][0], self.varsPos[idx][1])

    def displayMemory(self, memory):
        '''displayMemory
        :Version: 1.0.0
        
        Displays the memory on the right of the game screen
        
        As we can't display all the memory cells, we show a list containing the current memory adress. 
        But if it isn't in the list anymore, we refresh the list starting with the current one
        
        :param memory:
        :type memory: chip8CPU._UnsignedBitsArray
        
        '''

        replaceMemory = False 	# Boolean to check if we need to refresh the list
        pc = self.vars[3][1] 	# Gets the current PC value
        redSquare = 8 			# Size in pixels of the red square indicating the current memory adress

        try: 					# This try statement is used to check if the current PC adress is in the displayed memory adresses
            if(self.mem[pc]): 	# If it is already displayed, we do nothing and continue the function
                pass
        except KeyError: 		# If it isn't displayed, we will have to replace the memory displayed so we switch the boolean to True
            replaceMemory = True

        if replaceMemory: 	# If we need to refresh the displayed memory

            y = self.memY 							# We set the Y cursor at the available Y position given when initiating the window (because the memory list is displayed under the commands)
            x = self.XSize * self.upscaling + 20 	# Initial X cursor is set 20 pixels on the right of the game screen

            self.mem = {} 		# We reset the memory dictionnary storing the displayed memory adresses
            self.memPos = {} 	# We reset the positions dictionnary
            self.memSizes = {} 	# We reset the sizes dictionnary

            # Erase the whole memory panel
            # We start it at x-10 so we also erase the red indicator
            # The new black panel goes all the way to the right and bottom
            pygame.draw.rect(self.window, self.gameBg, (x-10, y, self.SXSize - x + 1, self.SYSize - y + 1))

            for idx, val in enumerate(memory): 	# For each value in memory (even those not displayed)
                if idx < pc: 	# If the adress being checked is inferior to the current PC one we pass it because we want to start displaying at our current PC value
                    continue
                if idx % 2 == 1: 	# If the adress being checked isn't an even number, we pass it because an adress stores an 8 bit value, and we are interested by displaying 16 bits for each one because the opcodes are in 16 bits
                    continue
                opcode = (val << 8) + memory[idx+1] 						# Calculate the opcode value
                text = "PC: #{:0=4X} || Op: #{:0=4X}".format(idx, opcode) 	# Define the text to display the adress and associated opcode
                size = self.font.size(text) 								# Save the size of the to-be-displayed text
                if y + size[1] > self.SYSize: 	# If we have reached the bottom of the window we won't display any new memory adress, neither this one
                    break
                if idx == pc: 																						# If the adress being checked is the current PC
                    self.PCPos = (x-10, y+(size[1]-redSquare)/2) 													# Calculate the position of the red indicator
                    pygame.draw.rect(self.window, self.debugSquareColor, (self.PCPos[0], self.PCPos[1], redSquare, redSquare)) 	# Draw the red square on the left of the adress line
                self.mem[idx] = val 							# Save the value associated to the adress being checked
                self.memPos[idx] = (x, y) 						# Save the X and Y position of the text
                self.memSizes[idx] = self.drawText(text, x, y) 	# Draw the text and save its size
                self.mem[idx+1] = memory[idx+1] 	# Also save the next memory value because we are skipping odd memory adresses because they are not displayed and we still need it to compare against new values
                y += size[1] 	# Move the Y cursor under the newly displayed text

        else: 	# If we don't need to refresh the displayed memory values

            for idx, val in self.mem.items(): 	# For each displayed value
                if idx % 2 == 1: 	# If it is an odd memory adress we skip it
                    continue

                if idx == pc: 	# If the adress being checked is the current PC
                    pygame.draw.rect(self.window, self.gameBg, (self.PCPos[0], self.PCPos[1], redSquare, redSquare)) 	# Erase the previous red indicator
                    newPCy = self.memPos[idx][1]+(self.memSizes[idx][1]-redSquare)/2 									# Calculate the new Y position of the indicator
                    self.PCPos = (self.PCPos[0], newPCy) 																# Save the new X and Y position of the indicator
                    pygame.draw.rect(self.window, self.debugSquareColor, (self.PCPos[0], self.PCPos[1], redSquare, redSquare)) 		# Draw the red square on the left of the adress line

                if self.mem[idx] != memory[idx] or self.mem[idx+1] != memory[idx+1]: 	# If the value of the adress being checked, or the next one (opcodes are 16 bits) changed, we refresh its value on screen
                    self.mem[idx] = memory[idx] 						# Save current and next value
                    self.mem[idx+1] = memory[idx+1]
                    opcode = (self.mem[idx] << 8) + self.mem[idx+1] 	# Calculate opcode

                    pos = self.memPos[idx] 	# Retrieve the position of current memory adress on the screen

                    pygame.draw.rect(self.window, self.gameBg, (pos[0], pos[1], self.memSizes[idx][0], self.memSizes[idx][1])) 	# Erase the currently displayed value

                    text = "PC: #{:0=4X} || Op: #{:0=4X}".format(idx, opcode) 	# Create the displayed text

                    self.memSizes[idx] = self.drawText(text, pos[0], pos[1]) 	# Draw the text and save its size

    def initCommands(self):
        '''initCommands
        :Version: 1.0.0
        
        Display the available commands
        
        :return: The Y position 10px under the line drawn at the bottom of commands menù
        :rtype: int
        '''
        initialX = self.XSize * self.upscaling + 10 	# Initial X is on the right of the game screen
        x = initialX 									# Sets the X cursor to the initial X value
        y = 10 											# Y cursor is on the top of the window

        y += self.drawText('ESC: Quit', x, y)[1] 			# We draw the commands text and move the Y cursor by the text's size
        y += self.drawText('F1: Change ROM', x, y)[1]
        y += self.drawText('F2: Reboot ROM', x, y)[1]
        y += self.drawText('F3: Pause / Unpause', x, y)[1]
        y += self.drawText('F4: Next step', x, y)[1]
        y += self.drawText('F5: Sound ON / OFF', x, y)[1]

        y += 10 																	# Move the cursor 10px downward
        pygame.draw.line(self.window, self.gameFg, (x, y), (self.SXSize - 10, y)) 	# To draw a separation line between commands and memory which is under

        return y+10 	# Return the Y position 10px under our drawn line

    def togglePause(self, pauseStatus):
        '''togglePause
        :Version: 1.0.0
        
        Display or remove the **PAUSED** label when the game is on pause
        
        :param pauseStatus: ``True`` when pause is on, otherwise ``False``
        :type pauseStatus: boolean 
        
        '''
        if(self.pause[0] != 0): 	# If the X position of the label is not 0, it means we already drew it once and even if it isn't actually displayed, we erase it with a black rectangle
            pygame.draw.rect(self.window, self.gameBg, (self.pause[0], self.pause[1], self.pause[2], self.pause[3])) 	# The black rectangle uses the X, Y position of the label and its size

        if pauseStatus: 	# If we want to display the label

            text = self.pauseText 						# Defining the text
            size = self.font.size(text) 				# Saving its size
            
            x = self.XSize * self.upscaling - size[0] 	# Calculating its position to be just under the bottom right corner of the game screen
            y = self.YSize * self.upscaling + 10
            
            size = self.drawText(text, x, y) 			# Displaying the text
            self.pause = (x, y, size[0], size[1]) 		# Saving its position and size in the object
            
    def toggleSound(self, audioStatus):
        '''toggleSound
        :Version: 1.0.0
        
        Display or remove the **MUTE** label when the user 
        
        :param audioStatus: ``True`` when audio is on, otherwise ``False``
        :type audioStatus: boolean 
        
        '''
        if(self.mute[0] != 0): 	# If the X position of the label is not 0, it means we already drew it once and even if it isn't actually displayed, we erase it with a black rectangle
            pygame.draw.rect(self.window, self.gameBg, (self.mute[0], self.mute[1], self.mute[2], self.mute[3])) 	# The black rectangle uses the X, Y position of the label and its size

        if not audioStatus: 	# If we want to display the label

            text = self.muteText 						# Defining the text
            size = self.font.size(text) 				# Saving its size
            
            x = self.XSize * self.upscaling - size[0] 	# Calculating its position to be just under the bottom right corner of the game screen
            y = self.YSize * self.upscaling + 30
            
            size = self.drawText(text, x, y) 			# Displaying the text
            self.mute = (x, y, size[0], size[1]) 		# Saving its position and size in the object



if __name__ == '__main__':
    pass
