# CIPPOTTO
**cippotto** is a(nother) **CHIP-8 Emulator** written in Python with Pygame.

## What is CHIP-8?
- CHIP-8 is an interpreted language, developed by **Joseph Weisbecker**. 
- It was made to allow easier video games programming for some computers in the mid of 1970.
- Most of the popular games of this era had their CHIP-8 version, we are talking about games like Pong, Breakout and Tetris.

## Why write a CHIP-8 emulator?
- Learning purposes and play with some CHIP-8 ROMS.
- Even though it isn't a real machine such as Vectrex, NES or Game Gear, it's like a simpler version of those.
- There are instructions to translate, registers and memory to manage, ROMs to load and a screen to display.

## Requirements
- Python 3
- Tkinter library to browse files and choose a ROM, on Debian based you can try `sudo apt install python3-tk`
- Pygame for rendering loaded game on screen and intercept keypress, you can install it using `pip3 install pygame`
- Numpy to play Audio, you can install it using `pip3 install numpy`
- SciPy to play Audio, you can install it using `pip3 install scipy`
- Sphinx is optional, useful if you want make or update documentation, you can install it using `pip3 install sphinx` and `pip3 install sphinx-mdinclude`
- **cippotto** is written and tested only on Linux, I don't know if it will work also on windows, I think yes with little or no code modifications

## Keyboard and keymap
- In CHIP-8 keyboard there are 16 available keys in a 4x4 config
- Original CHIP-8 keyboard is:

	```
	1   2   3   C
	4   5   6   D
	7   8   9   E
	A   0   B   F
	```
- **cippotto** keymap for **QWERTY** keyboards is:

	```
	1   2   3   4
	Q   W   E   R
	A   S   D   F
	Z   X   C   V
	```
- **cippotto** keymap for **AZERTY** keyboards is:

	```
	1   2   3   4
	A   Z   E   R
	Q   S   D   F
	W   X   C   V
	```

## Emulator buttons
- Game buttons are read from **QWERTY** or **AZERTY** keymaps.
- Note that not all games use all available keymap keys
- There are other service and/or option keys usable during gameplay:
    - ``ESC``: Quit
    - ``F1``: Change ROM
    - ``F2``: Reboot ROM
    - ``F3``: Pause / Unpause
    - ``F4``: Next step
    - ``F5``: Sound ON / OFF
    - ``F6``: Debug ON / OFF 

## How to play
- To start **cippotto** run the following command:

	```
	python3 main.py
	```
- Press `SELECT ROM` button to open a file dialog
- Select wanted ROM from file dialog and open it
- Play with selected ROM and/or do experiments and/or write code

## Project documentation
- The HTML documentation is generated using `Sphinx 5.1.1`
- To navigate and read it locally open `Docs/build/html/index.html` with a browser and enjoy
- You can read **cippotto** documentation also online, *maybe not even up to date*:
	- <a href="https://www.mangaraven.com/coolprojects/cippotto/docs/" target="_blank">Click here to read online cippotto documentation</a>

## ToDo
Read the docs and/or read the code to find some todos annotations

## Compatibility table
This is a list of tested ROMS, maybe there are a lot of ROMS that I haven't tested
- **WORKING** `15 Puzzle [Roger Ivie] (alt).ch8` MD5 `45f9c5f9cd88a744b67f368a0c1b925c`
- **WORKING** `15 Puzzle [Roger Ivie].ch8` MD5 `aaf7c09c7f5f952d0f0c326f9e01b5c0`
- **NOT WORKING** `Addition Problems [Paul C. Moews].ch8` MD5 `daf2e3f0657e07a8898a78434007d11c`
- **WORKING** `Airplane.ch8` MD5 `02c2993360d186d3ae265ee7388481de`
- **WORKING** `Animal Race [Brian Astle].ch8` MD5 `46497c35ce549cd7617462fe7c9fc284`
- **WORKING ???** `Astro Dodge [Revival Studios, 2008].ch8` MD5 `a7b171e6f738913f89153262b01581ba`
- **WORKING ???** `BC_test.ch8` MD5 `c2357b0be2aad042007ee3f6108fa2cc`
- **WORKING ???** `Biorhythm [Jef Winsor].ch8` MD5 `c5103d14ce839a06aa7e7a562807e21b`
- **WORKING ???** `Blinky [Hans Christian Egeberg, 1991].ch8` MD5 `e1c84e1156174070661c1f6ca0481ba5`
- **WORKING ???** `Blinky [Hans Christian Egeberg] (alt).ch8` MD5 `86d5472509cf68c68ea9c2728f4a7ff3`
- **WORKING** `Blitz [David Winter].ch8` MD5 `8180b836eeb629ba93583519a5fb7b38`
- **WORKING** `Bowling [Gooitzen van der Wal].ch8` MD5 `b56e0e6e3930011049fcf6cf3384e964`
- **WORKING** `Breakout (Brix hack) [David Winter, 1997].ch8` MD5 `3b26819c641e08cce4559aa1c68b09b1`
- **WORKING** `Breakout [Carmelo Cortez, 1979].ch8` MD5 `1d2a47947d6d46b1ceb41cf38b8cfc7e`
- **WORKING** `Brick (Brix hack, 1990).ch8` MD5 `008335a1292130403ddd5f222fa56944`
- **WORKING** `Brix [Andreas Gustafsson, 1990].ch8` MD5 `d677c1b9de941484d718799aebafebf3`
- **WORKING** `Cave.ch8` MD5 `bea7fdce1ef1733f9298dbbe2257cb9c`
- **WORKING** `Coin Flipping [Carmelo Cortez, 1978].ch8` MD5 `2facc7c3b181e9088fe86cf5504d83d7`
- **NOT WORKING** `Connect 4 [David Winter].ch8` MD5 `29c5bf2d8f754dfe923923934513fb2d`
- **NOT WORKING** `Craps [Camerlo Cortez, 1978].ch8` MD5 `a47bec324b82084467ae4368df8813d9`
- **WORKING ???** `Deflection [John Fort].ch8` MD5 `2c95b08ad4c1d6c7c35f937ad6e84822`
- **WORKING** `Figures.ch8` MD5 `09c9ad9a9810d7e35f7be8f97edc146e`
- **WORKING** `Filter.ch8` MD5 `e0d2f1471b860bb99ce3d5a38d6eb2ac`
- **WORKING** `Guess [David Winter] (alt).ch8` MD5 `0e1e9fcffa803d75d81bc42f25e4b95b`
- **WORKING** `Guess [David Winter].ch8` MD5 `896831cac662cb5d315f9141d826eb5c`
- **NOT WORKING** `Hidden [David Winter, 1996].ch8` MD5 `d3f623110c962a28b86fc63e64bf33f0` 
    - *Seem that isn't recognized keyup event*
- **NOT WORKING** `Hi-Lo [Jef Winsor, 1978].ch8` MD5 `4bd002d80da3d3070851c91935f7fb96`
- **NOT WORKING** `Kaleidoscope [Joseph Weisbecker, 1978].ch8` MD5 `f330d48b32a2fd77cf41939f1d40ac06`
    - *keypad not working*
- **WORKING** `Landing.ch8` MD5 `9072c134d284f809fcf15f07a44aac01`
- **WORKING** `Lunar Lander (Udo Pernisz, 1979).ch8` MD5 `7fc7a749d62813bd670b69620bca8f40`
- **NOT WORKING** `Mastermind FourRow (Robert Lindley, 1978).ch8` MD5 `daeb7c550c44d2953a0e9c9aab352a11`
    - *keypad not working*
- **WORKING** `Merlin [David Winter].ch8` MD5 `51ca9370c219355e4b61bb13c3e4fb37`
- **WORKING** `Missile [David Winter].ch8` MD5 `ccf8de6e6fe799b56e27b6761251b107`
- **WORKING** `Most Dangerous Game [Peter Maruhnic].ch8` MD5 `d8b3ccd5151d4b08edc0d5d87bb70603`
- **WORKING ???** `Nim [Carmelo Cortez, 1978].ch8` MD5 `0f37fce6fe5398aa4280d7e066e59cee`
- **WORKING** `Paddles.ch8` MD5 `d9f90b02aaa5aa2f00485615a3f0eafe`
- **WORKING** `Pong (1 player).ch8` MD5 `d7c0a76147c5f16bb498aa410f1f21f2`
- **WORKING** `Pong 2 (Pong hack) [David Winter, 1997].ch8` MD5 `dc0cf425bef93a9ecf09fff4565e7410`
- **WORKING** `Pong (alt).ch8` MD5 `e66b29e565b741710eb6d48a3c6757ad`
- **WORKING** `Pong [Paul Vervalin, 1990].ch8` MD5 `873436fe95dbed9c43c41673e9edfd4f`
- **WORKING ???** `Programmable Spacefighters [Jef Winsor].ch8` MD5 `fb9ea0cde210ae2f02565a5ff92cc3f7`
- **WORKING** `Puzzle.ch8` MD5 `28a67d7f503fe66b999b189848127678`
- **WORKING** `Reversi [Philip Baltzer].ch8` MD5 `a1e29daa2a39cf87aa9e81b5b9cb0a2e`
- **WORKING** `Rocket [Joseph Weisbecker, 1978].ch8` MD5 `bb64919f8eaef896e41ae94561b05fc8`
- **WORKING ???** `Rocket Launcher.ch8` MD5 `e551a30b32b45451b0b67931744244e1`
- **WORKING ???** `Rocket Launch [Jonas Lindstedt].ch8` MD5 `550dfbf87cf1dc62104e22def4378b3b`
- **WORKING ???** `Rush Hour [Hap, 2006] (alt).ch8` MD5 `48a8fec113093ceec5e41986278852ea`
- **WORKING ???** `Rush Hour [Hap, 2006].ch8` MD5 `48abd3e02413a34f0591c1c7f9e7a60b`
- **WORKING** `Russian Roulette [Carmelo Cortez, 1978].ch8` MD5 `d41fd42defd61a507d66231c67acac6a`
- **NOT WORKING** `Sequence Shoot [Joyce Weisbecker].ch8` MD5 `30525189275852cc6300072cc792131e`
- **WORKING** `Shooting Stars [Philip Baltzer, 1978].ch8` MD5 `8a202caf3b4f0fe3194276b8f8e508b7`
- **WORKING** `Slide [Joyce Weisbecker].ch8` MD5 `c6fa9e7a3f6dba491d1dfc1fe7b5df4e`
- **WORKING** `Soccer.ch8` MD5 `f033687b4cf613cf3888a3345b64d2c6`
- **WORKING** `Space Flight.ch8` MD5 `dba8c50789808184c96f0173930bc81e`
- **WORKING** `Space Intercept [Joseph Weisbecker, 1978].ch8` MD5 `2d7ab275b39ca46d9c7228b9cee46b63`
- **WORKING** `Space Invaders [David Winter] (alt).ch8` MD5 `4fe20b951dbc801d7f682b88e672626c`
- **WORKING** `Space Invaders [David Winter].ch8` MD5 `a67f58742cff77702cc64c64413dc37d`
- **WORKING ???** `Spooky Spot [Joseph Weisbecker, 1978].ch8` MD5 `a5e64d9255b236864cc49234ceefce8f`
- **WORKING** `Squash [David Winter].ch8` MD5 `4a1b1a19be22acd4b82b68dd2399fe68`
- **WORKING** `Submarine [Carmelo Cortez, 1978].ch8` MD5 `f2ed4039d738eb118edd9b9de52960e6`
- **NOT WORKING** `Sum Fun [Joyce Weisbecker].ch8` MD5 `c64855dc68772301c8f96ddcc6a0b1d0`
- **WORKING ???** `Syzygy [Roy Trevino, 1990].ch8` MD5 `9440bf0cda416b3ecf568cfaa349d4fc`
- **WORKING** `Tank.ch8` MD5 `0ac0fd0d309c21a6cad14bb28217f5e4`
- **WORKING ???** `Tapeworm [JDR, 1999].ch8` MD5 `dadaaf440809732d51485a2bc9410781`
- **NOT WORKING** `test.ch8` MD5 `0d4db5502861bf78146ce2fb90cf5083`
- **WORKING** `TESTOPCODE.ch8` MD5 `0911aef35bfa6c7f9a8f17feb1bf3359`
- **WORKING** `Tetris [Fran Dachille, 1991].ch8` MD5 `aef4fc8c2a5e8431f5e0736ab281f2ee`
- **WORKING ???** `Tic-Tac-Toe [David Winter].ch8` MD5 `4e5b1b1730cfa85e68a43e4766814a98`
- **WORKING** `Timebomb.ch8` MD5 `58695661362197cb3a21aebb306c61a4`
- **WORKING ???** `Tron.ch8` MD5 `0c439070315a7926b79a28202bb7b260`
- **WORKING** `UFO [Lutz V, 1992].ch8` MD5 `7e35f93c5a788e7e0027c78e8b76c8fb`
- **NOT WORKING** `Vers [JMN, 1991].ch8` MD5 `486bddc6e25e378838ff3e944db9a443`
    - *When both players exit from the left or bottom screen the game must be rerstarted*
- **WORKING** `Vertical Brix [Paul Robson, 1996].ch8` MD5 `9ec0fe6b275220f2c8821889a5a7fcab`
- **WORKING** `Wall [David Winter].ch8` MD5 `8ef214bf9bcf8d038702af3dccb60a01`
- **WORKING** `Wipe Off [Joseph Weisbecker].ch8` MD5 `41d64f82dc3c457e4f8543e081ae8e85`
- **WORKING** `Worm V4 [RB-Revival Studios, 2007].ch8` MD5 `4c0c381ac4942462b41876bb75e8a20a`
- **WORKING** `X-Mirror.ch8` MD5 `e868f362a4a91cf331753c55545dc271`
- **WORKING** `ZeroPong [zeroZshadow, 2007].ch8` MD5 `659eb0a2009ef9c096397ded7f72ba8f`

#### Resources used to write this emulator
- <a href="https://en.wikipedia.org/wiki/CHIP-8" target="_blank">CHIP-8 - Wikipedia</a> - Nice preview of what CHIP-8 is
- <a href="http://www.cs.columbia.edu/~sedwards/classes/2016/4840-spring/designs/Chip8.pdf" target="_blank">CHIP-8 - Columbia University</a> - CHIP-8 Design Specification - A great document that deals in depth with CHIP-8
- <a href="https://github.com/SuzieQQ/Chip8-Emulator" target="_blank">CHIP-8 by SuzieQQ</a> - CHIP-8 Emulator written in C++
- <a href="https://github.com/ThibaultNocchi/MyChip8Emulator/blob/master/CHIP8.py" target="_blank">CHIP-8 by ThibaultNocchi</a> - CHIP-8 Emulator written in Python in one big file with a great register and memory debug out of the box

#### Legal notices
**CopyLeft 2022 n4d46t3m netcat**

<br>
<br>
<br>
