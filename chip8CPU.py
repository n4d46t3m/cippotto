'''

CHIP-8 CPU Emulator written in Python by netcat n4d4s

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

CopyLeft 2022

'''

from random import randint

class _UnsignedBitsArray:
    '''
    Class designed to be a list of cellsNbr values, which are cellLength bits unsigned integers.
    '''
    
    def __init__(self, cellLength, cellsNbr):
        '''
        _UnsignedBitsArray __init__
            Init _UnsignedBitsArray class
            
        Initialize the list of unsigned values setting ``self._arr`` ``self._cellLength`` and ``self._cellsNbr``
            
        :param cellLength: Cell length in bits
        :type cellLength: int
        :param cellsNbr: List length in bits
        :type cellsNbr: int
        '''
        self._cellLength = abs(cellLength) 	# Storing the absolute value of the cell length
        self._cellsNbr = abs(cellsNbr)      # Storing the absolute value of list length
        self._arr = [0] * self._cellsNbr 	# Initialize the list of unsigned values

    def __str__(self):
        '''__str__
        :Version: 1.0.0
        
        Displays each cell with its index and value, in hexadecimal
        
        :return: Formatted text to display each cell with its index and value in hexadecimal
        :rtype: string
        '''
        result = ''
        for idx, val in enumerate(self._arr):
            result += "{} : {}\n".format(hex(idx), hex(val))
        return result

    def __getitem__(self, idx):
        '''__getitem__
        :Version: 1.0.0
        
        Getting a value is straightforward, we do not treat it
        
        :param idx: The index of the list
        :type idx: int
        
        :return: The value of the selected cell
        :rtype: int
        '''
        return self._arr[idx]

    def __setitem__(self, idx, value):
        '''__setitem__
        :Version: 1.0.0
        
        When setting a value it stays withing the boudaries of an unsigned cellLength bits integer. 
        If it overflows we just truncate the Most Significant Bits to take the length down to cellLength. The modulo does it well.
        
        :param idx: The cell that should be set
        :type idx: int
        :param value: The value that must be stored in ``self._arr[idx]`` using the formula ``value % (2**self._cellLength)``
        :type value: int
        '''
        self._arr[idx] = value % (2**self._cellLength)

class CPU:
    
    def __init__(self):
        '''
        CPU __init__
            Init CPU class
        '''  
        self.initVars() 		# Initialize all the emulator variables which are resetted when changing a ROM, so we don't have to destroy our CHIP8 object when rebooting the current ROM
        
    def initVars(self):
        '''initVars
        :Version: 1.0.0
        
        Initializes the variables used to run the emulator
        '''
        self.speed = 1000   # Speed of CPU in Hz (Best use cases like 1000)
        self.timerHZ = 60   # Delay and sound timers should always tick down at 60Hz
        self.delayInterval = round((1/self.timerHZ)*1000)   # Used to sets up the 60Hz timer
        self.currentOpcode = 0x0000 # This will contain the opcode that should be processed
        self.n = 0 # It will contains current n
        self.x = 0 # It will contains current x
        self.y = 0 # It will contains current y
        self.startMemory = 0x200                    # Default adress to store the program (the given ROM)
        self.memory = _UnsignedBitsArray(8, 4096) 	# Memory of 4096 * 8 bits
        self.V = _UnsignedBitsArray(8, 16) 			# V[X] register. 16 * 8 bits
        self.I = 0 									# I register. 16-bit index register. This register is generally used to store memory addresses, so only the lowest (rightmost) 12 bits are usually used
        self.delay_timer = 0 						# Delay Timer. Should be decremented to 0 at a rate of 60Hz
        self.sound_timer = 0 						# Sound Timer. Should be decremented to 0 at a rate of 60Hz
        self.PC = 512 								# Program Counter. 16-bit pseudo register which isn't accessible from CHIP-8 programs. Start at 0x200. Is used to store the currently executing address
        self.SP = 0 								# Stack Pointer. 8-bit pseudo register which isn't accessible from CHIP-8 programs
        self.stack = _UnsignedBitsArray(16, 16)     # Stack pile. 16 * 16 bits. The stack is used to store return addresses when subroutines are called

        self.loadFonts() 							# Load fonts in memory
        
    def loadFonts(self):
        '''loadFonts
        :Version: 1.0.0
        
        Loads the Chip 8 fonts at the beginning of the memory
        
        Each character is defined by a sprite of 5 lines of 4 pixels.
        
        In Chip 8, those characters are stored as 5 bytes in the memory, and each byte has its 4 MSB representing the 4 pixels. The 4 LSB are set to 0
        
        In this script we wrote those bytes in a list so it'd be easier to load them (and write the code) this way, instead of setting each cell of memory one by one. We are lazy.
        
        This will set ``self.memory``
        '''

        fonts = [
            0xF0, 0x90, 0x90, 0x90, 0xF0,   # 0
            0x20, 0x60, 0x20, 0x20, 0x70,   # 1
            0xF0, 0x10, 0xF0, 0x80, 0xF0,   # 2
            0xF0, 0x10, 0xF0, 0x10, 0xF0,   # 3
            0x90, 0x90, 0xF0, 0x10, 0x10,   # 4
            0xF0, 0x80, 0xF0, 0x10, 0xF0,   # 5
            0xF0, 0x80, 0xF0, 0x90, 0xF0,   # 6
            0xF0, 0x10, 0x20, 0x40, 0x40,   # 7
            0xF0, 0x90, 0xF0, 0x90, 0xF0,   # 8
            0xF0, 0x90, 0xF0, 0x10, 0xF0,   # 9
            0xF0, 0x90, 0xF0, 0x90, 0x90,   # A
            0xE0, 0x90, 0xE0, 0x90, 0xE0,   # B
            0xF0, 0x80, 0x80, 0x80, 0xF0,   # C
            0xE0, 0x90, 0x90, 0x90, 0xE0,   # D
            0xF0, 0x80, 0xF0, 0x80, 0xF0,   # E
            0xF0, 0x80, 0xF0, 0x80, 0x80    # F
        ]

        for idx, val in enumerate(fonts):  # For each byte
            self.memory[idx] = val  # We put it into the memory starting from the adress 0x0 which is easier for us, but it could be anywhere between 0x00 and 0x1FF because those memory cells are free and we only need 80 of them.

    def getCurrentOpcode(self): 	
        '''getCurrentOpcode
        :Version: 1.0.0
        
        Retrieves the value of current opcode

        We retrieve the value in the memory for the current ``PC``, and shift it 8 times to the left because it represents the 8 MSB of 16 bits message
        
        We then add the value for the next memory cell, without any shifting because those are the 8 LSB
        
        We now have our current opcode
        
        :return: An opcode as int
        :rtype: int
        '''
        self.currentOpcode = (self.memory[self.PC] << 8) + self.memory[self.PC+1]
        # Those 3 operations retrieve the 12 LSB of our opcode, in group of 4 bits, which correspond to an hexadecimal character. Usually opcodes are treated in hexadecimal, it is easier to visualize
        # This is to get the differents potential parameters of the opcode
        # So for the opcode Dxyn where we need x, y and n, we would get all of these 3 in variables
        # To do this, we use a combination of masking and shifting.
        # I don't know how to explain, but with the masking we cancel all bits which we dont need.
        # With shifting, if your bits aren't at the LSB position, their integer value doesn't represent their 4 bits value, so we shift them to put them in the LSB position
        self.n = self.currentOpcode & 0x000F 			# 4th hexadecimal character (here it would be the n)
        self.y = (self.currentOpcode & 0x00F0) >> 4 	# 3rd hexadecimal character (here it would be the y)
        self.x = (self.currentOpcode & 0x0F00) >> 8 	# 2nd hexadecimal character (here it would be the x)
        return self.currentOpcode

    def OP_00E0(self):
        '''OP_00E0
        :Version: 1.0.0
        
        Clears the screen
        '''
        #print('00E0')#DEBUG
        print('Cleaning the screen')#DEBUG
        self.screen.clear()

    def OP_00EE(self):
        '''OP_00EE
        :Version: 1.0.0
        
        Returns from a subroutine
        '''
        #print('00EE')#DEBUG
        if(self.SP > 0): 					# If our stack pointer is above 0
            self.SP -= 1 					# We decrement it to return from where we came from
            self.PC = self.stack[self.SP] 	# We set the PC to the last stack value we did (so the CALL instruction). This won't fall in an infinite loop because at the end of each opcode treatment, we increase the PC to reach next instruction

    def OP_1NNN(self):
        '''OP_1NNN
        :Version: 1.0.0
        
        Jumps to the location ``NNN`` in the memory
        '''
        #print('1NNN')#DEBUG
        self.PC = self.currentOpcode & 0x0FFF # In origine questa riga non c'era e quella sotto era decommentata
        #self.PC = (self.x<<8) + (self.y<<4) + self.n 	# Sets the PC to the value of NNN. We need to shift our values in order to create the real value
        self.PC -= 2 						# Remember to decrement the PC by a step, because at the end of this instruction it'll be incremented by a step and we won't be where we wanted to, we will be 1 step ahead.

    def OP_2NNN(self):
        '''OP_2NNN
        :Version: 1.0.0
        
        Calls subroutine ``NNN``
        
        This is like a jump but we save the current ``PC`` to return at the end of the routine
        '''
        #print('2NNN')#DEBUG
        self.stack[self.SP] = self.PC 		# Store the current PC in the stack
        if(self.SP < 15): 					# If the stack pointer isn't being overflowed (only 8 bits max) we increase it
            self.SP += 1
        self.PC = self.currentOpcode & 0x0FFF # In origine questa riga non c'era e quella sotto era decommentata
        #self.PC = (self.x<<8) + (self.y<<4) + self.n 	# As previously for the jump, we set the new PC by shifting the different values
        self.PC -= 2 						# And we remember to decrement the PC by a step
        
    def OP_3XKK(self):
        '''OP_3XKK
        :Version: 1.0.0
        
        Skip next instruction if ``V[X]`` and ``KK`` are equal
        '''
        #print('3XKK')#DEBUG
        #if(self.V[self.x] == (self.currentOpcode & 0x00FF)):
        if(self.V[self.x] == ((self.y<<4)+self.n)): 	# Comparison, remember to shift the KK values
            self.PC += 2 					            # If those are equal, we increase the PC by a step.
        
    def OP_4XKK(self):
        '''OP_4XKK
        :Version: 1.0.0
        
        Skip next instruction if ``V[X]`` and ``KK`` are not equal
        '''
        #print('4XKK')#DEBUG
        #if(self.V[self.x] != (self.currentOpcode & 0x00FF)):    # Same as before, just with a not equal
        if(self.V[self.x] != ((self.y<<4)+self.n)):             # Same as before, just with a not equal
            self.PC += 2
            
    def OP_5XY0(self):
        '''OP_5XY0
        :Version: 1.0.0
        
        Skip next instruction instruction if the value of register ``VX`` is equal to the value of register ``VY``
        '''
        #print('5XY0')#DEBUG
        if((self.currentOpcode & 0x000F) == 0x0000):    # 5XY0 : skip next instruction if V[X] = V[Y]
            if(self.V[self.x] == self.V[self.y]):       # Same as before, but comparison between two registers
                self.PC += 2
            
    def OP_6XKK(self):
        '''OP_6XKK
        :Version: 1.0.0
        
        Sets the value ``KK`` into ``V[X]``
        '''
        #print('6XKK')#DEBUG
        #print('OP_6XKK',type(self.currentOpcode),hex(self.currentOpcode),hex(self.currentOpcode & 0x00FF))#DEBUG
        self.V[self.x] = self.currentOpcode & 0x00FF    # In origine questa riga non c'era e quella sotto era decommentata
        #self.V[self.x] = (self.y << 4) + self.n        # Same as line above. Remember to shift the first K to be able to add the second K
        
    def OP_7XKK(self):
        '''OP_7XKK
        :Version: 1.0.0
        
        Adds ``KK`` to ``V[X]``
        '''
        #print('7XKK')#DEBUG
        #print('OP_7XKK',type(self.currentOpcode),hex(self.currentOpcode),hex(self.currentOpcode & 0x00FF))#DEBUG
        self.V[self.x] += self.currentOpcode & 0x00FF   # In origine questa riga non c'era e quella sotto era decommentata
        #self.V[self.x] += (self.y << 4) + self.n       # Same as line above. Remember to shift the first K to be able to add the second K
     
    def OP_8xxx(self):
        '''OP_8xxx
        :Version: 1.0.0
        
        Execute ``0x8000`` instructions
        
            **Known instructions**
                - ``8XY0`` : sets ``V[X] = V[Y]`` See :py:func:`CPU._OP_8XY0()`
                - ``8XY1`` : sets ``V[X] = V[X] OR V[Y]`` See :py:func:`CPU._OP_8XY1()`
                - ``8XY2`` : sets ``V[X] = V[X] AND V[Y]`` See :py:func:`CPU._OP_8XY2()`
                - ``8XY3`` : sets ``V[X] = V[X] XOR V[Y]`` See :py:func:`CPU._OP_8XY3()`
                - ``8XY4`` : adds ``V[Y]`` into ``V[X]`` See :py:func:`CPU._OP_8XY4()`
                - ``8XY5`` : sets ``V[X] = V[X] - V[Y]`` and sets ``V[0xF]`` to 0 if we "underflow", else it is 1. See :py:func:`CPU._OP_8XY5()`
                - ``8XY6`` : sets ``V[0xF]`` to the LSB of ``V[Y]``, divides ``V[Y]`` by 2 and puts it into ``V[X]`` See :py:func:`CPU._OP_8XY6()`
                - ``8XY7`` : sets ``V[X] = V[Y] - V[X]`` See :py:func:`CPU._OP_8XY7()`
                - ``8XYE`` : sets ``V[0xF]`` to the MSB of ``V[Y]``, multiplies ``V[Y]`` by 2 and puts it into ``V[X]`` See :py:func:`CPU._OP_8XYE()`
                
        '''
        result = self.currentOpcode & 0x000F
        if(result == 0x0000):   # 8XY0 : sets V[X] = V[Y]
            self._OP_8XY0()
        elif(result == 0x0001): # 8XY1 : sets V[X] = V[X] OR V[Y]. It is a bitwise operation. If we do 0101 OR 1100 it'll do 1101. We do an OR on each bit
            self._OP_8XY1()
        elif(result == 0x0002): # 8XY2 : sets V[X] = V[X] AND V[Y]. Bitwise operation
            self._OP_8XY2()
        elif(result == 0x0003): # 8XY3 : sets V[X] = V[X] XOR V[Y]. Bitwise operation
            self._OP_8XY3()
        elif(result == 0x0004): # 8XY4 : adds V[Y] into V[X]. Sets V[0xF] (the last V register) to 1 if the addition is overflowing (if we have more than 8 bits, which the max value is 255). Else it is 0. And if we overflow, we just remove 256 from the number -> 258 becomes 2
            self._OP_8XY4()
        elif(result == 0x0005): # 8XY5 : V[X] = V[X] - V[Y] and sets V[0xF] to 0 if we "underflow", else it is 1.
            self._OP_8XY5()
        elif(result == 0x0006): # 8XY6 : sets V[0xF] to the LSB of V[Y], divides V[Y] by 2 and puts it into V[X]
            self._OP_8XY6()
        elif(result == 0x0007): # 8XY7 : V[X] = V[Y] - V[X]. Same as 8XY5, but VX and VY are reverted in the operation
            self._OP_8XY7()
        elif(result == 0x000E): # 8XYE : sets V[0xF] to the MSB of V[Y], multiplies V[Y] by 2 and puts it into V[X]
            self._OP_8XYE()
    
    def _OP_8XY0(self):
        '''_OP_8XY0
        :Version: 1.0.0
        
        Sets ``V[X] = V[Y]``
        '''
        self.V[self.x] = self.V[self.y] 	# No shifting needed
        
    def _OP_8XY1(self):
        '''_OP_8XY1
        :Version: 1.0.0
        
        Sets ``V[X] = V[X] OR V[Y]``. It is a bitwise operation. If we do 0101 OR 1100 it'll do 1101. We do an OR on each bit
        '''
        self.V[self.x] |= self.V[self.y] 	# In Python a bitwise OR is done with the | symbol, and we can use its shortcut which is like self.V[self.x] = self.V[self.x] | self.V[self.y]
        
    def _OP_8XY2(self):
        '''_OP_8XY2
        :Version: 1.0.0
        
        Sets ``V[X] = V[X] AND V[Y]``. Bitwise operation
        '''
        self.V[self.x] &= self.V[self.y] 	# Python symbol for a bitwise AND is &
        
    def _OP_8XY3(self):
        '''_OP_8XY3
        :Version: 1.0.0
        
        Sets ``V[X] = V[X] XOR V[Y]``. Bitwise operation
        '''
        self.V[self.x] ^= self.V[self.y] 	# Python symbol for a bitwise XOR is ^
      
    def _OP_8XY4(self):
        '''_OP_8XY4
        :Version: 1.0.0
        
        Adds ``V[Y]`` into ``V[X]``.
        
        Sets ``V[0xF]`` (the last V register) to 1 if the addition is overflowing 
        (if we have more than 8 bits, which the max value is 255). Else it is 0.
        
        And if we overflow, we just remove 256 from the number -> 258 becomes 2
        '''
        self.V[0xF] = 0                                 # Resets the carry
        if((self.V[self.x] + self.V[self.y]) > 0xFF):   # If we overflow
            self.V[0xF] = 1                             # We set the carry to 1
        self.V[self.x] += self.V[self.y]                # We add V[Y] into V[X]. We don't care about the overflow because it is treated by our nice UnsignedBitsArray
        
    def _OP_8XY5(self):
        '''_OP_8XY5
        :Version: 1.0.0
        
        ``V[X] = V[X] - V[Y]`` and sets ``V[0xF]`` to 0 if we "underflow", else it is 1
        '''
        self.V[0xF] = 1                         # Resets the borrow
        if(self.V[self.x] < self.V[self.y]):    # If we "underflow"
            self.V[0xF] = 0                     # We set the borrow to 0
        self.V[self.x] -= self.V[self.y]        # Do the operation
        
    def _OP_8XY6(self):
        '''_OP_8XY6
        :Version: 1.0.0
        
        Sets ``V[0xF]`` to the **LSB** of ``V[Y]``, divides ``V[Y]`` by 2 and puts it into ``V[X]``
        '''
        self.V[0xF] = self._getLSB(self.V[self.y]) 	# Getting the LSB thanks to our great function above
        self.V[self.x] = (self.V[self.y] >> 1) 		# Dividing by 2 with binary values is the same as shifting 1 bit to the right. That is why we get the LSB beforehand, because we want to know what it was before we deleted it
        
    def _OP_8XY7(self):
        '''_OP_8XY7
        :Version: 1.0.0
        
        ``V[X] = V[Y] - V[X]``. Same as ``8XY5``, but ``VX`` and ``VY`` are reverted in the operation
        '''
        self.V[0xF] = 1
        if(self.V[self.y] < self.V[self.x]):
            self.V[0xF] = 0
        self.V[self.x] = self.V[self.y] - self.V[self.x]
        
    def _OP_8XYE(self):
        '''_OP_8XYE
        :Version: 1.0.0
        
        Sets ``V[0xF]`` to the MSB of ``V[Y]``, multiplies ``V[Y]`` by 2 and puts it into ``V[X]``
        '''
        self.V[0xF] = self._getMSB(self.V[self.y], 8) 	# Getting the MSB. 8 represents the max length of our binary value.
        self.V[self.x] = (self.V[self.y] << 1) 			# Multiplying by 2 with binary values is like shifting 1 bit to the left. Same as for the division, we want to know what was the deleted bit
      
    def OP_9XY0(self):
        '''OP_9XY0
        :Version: 1.0.0
        
        Skip next instruction if the value of register ``VX`` is not equal to the value of register ``VY``
        '''
        if((self.currentOpcode & 0x000F) == 0x0000): 	# 9XY0 : skip next inscrution if V[X] != V[Y]
            if(self.V[self.x] != self.V[self.y]): 	# It works the same as those "skip instruction if" opcodes we did before
                self.PC += 2
                
    def OP_ANNN(self):
        '''OP_ANNN
        :Version: 1.0.0
        
        Sets ``I = NNN``
        '''            
        self.I = (self.x<<8) + (self.y<<4) + self.n 	# Always remember to shift our different N
        
    def OP_BNNN(self):
        '''OP_BNNN
        :Version: 1.0.0
        
        Jumps to location ``NNN + V[0]``
        '''
        self.PC = (self.x<<8) + (self.y<<4) + self.n + self.V[0x0] 	# We set the PC to NNN + V[0]
        self.PC -= 2    # Like our other jumps, we need to decrement the PC in order to reach the instruction we want
        
    def OP_CXKK(self):
        '''OP_CXKK
        :Version: 1.0.0
        
        Sets ``V[X]`` = (random byte) ``AND KK``
        '''
        self.V[self.x] = randint(0, 255) & ((self.y << 4) + self.n) # Our random byte is just a value between 0 and 255, and we do a bitwise AND with KK. Beware of the bit shifting
        
    def DXYN(self,screenPixels):
        '''DXYN
        :Version: 1.0.0
        
        Displays a sprite of length N at coordinates (``V[X]``, ``V[Y]``) from the memory starting at ``I``
        
        :param screenPixels: list of screen pixels that are on or off
        :type screenPixels: list of lists that can contain 0 or 1
        '''
        self.V[0xF] = 0 	# V[0xF] stores 0 by default, and 1 if during the instruction we erase a pixel (setting it to black, or 0)

        for i in range(self.n): 	# For each line of our sprite
            line = self.memory[self.I+i]    # We are reading the sprite starting from the I adress and increasing it for each line
            currentY = self.V[self.y] + i   # We set the initial Y axis position to the one given by V[Y] and we add to it the i counter to get the current line position

            if currentY < len(screenPixels[0]): # If we are in available height of the screen
                for j in range(8):  # A sprite is 8 bits in width
                    currentX = self.V[self.x] + j   # Sets the current X position to the value given by V[X] and adding the j counter value
                    
                    if currentX < len(screenPixels):    # If we are in available width of the screen
                        mask = 0x1 << (7-j) 										# Mask to be used to retrieve the wanted bit in the line (which is a byte). If I want the MSB bit, j is at 0, so we get a mask of 0b10000000. Applying it with a bitwise AND and shifting will get us our bit.
                        newBit = (mask & line) >> (7-j) 							# Getting the bit by applying the mask, and shifting it back to the LSB position
                        result = newBit ^ self.screen.pixels[currentX][currentY] 	# A new pixel is decided by doing a XOR between the current state of the pixel, and the value wanted by the sprite
                        screenPixels[currentX][currentY] = result                   # We set the result of the XOR to the screen

                        if(screenPixels[currentX][currentY] == 0 and newBit == 1):  # If we erased it (with a XOR, it happens when the pixel is erased and we wanted to apply a value of 1)
                            self.V[0xF] = 1                                         # We set V[0xF] to 1 as described
                            
    def OP_Exxx(self):
        '''OP_Exxx
        :Version: 1.0.0
        
        Execute ``0xE000`` instructions
        
            **Known instructions**
                - ``EX9E`` : skips next instruction if the key of value ``V[X]`` is pressed. See :py:func:`CPU._OP_EX9E()`
                - ``EXA1`` : skips next instruction if the key of value ``V[X]`` isn't pressed. See :py:func:`CPU._OP_EXA1()`
        '''
        result = self.currentOpcode & 0x000F
        if(result == 0x000E):   # EX9E : skips next instruction if the key of value V[X] is pressed
            self._OP_EX9E()
        elif(result == 0x0001): # EXA1 : skips next instruction if the key of value V[X] isn't pressed
            self._OP_EXA1()
                
    def _OP_EX9E(self):
        '''_OP_EX9E
        :Version: 1.0.0
        
        Skips next instruction if the key of value ``V[X]`` is pressed
        '''
        if self.key[self.V[self.x]] == 1:   # Same as always, we do the if and then we increase the PC
            self.PC += 2

    def _OP_EXA1(self):
        '''_OP_EXA1
        :Version: 1.0.0
        
        Skips next instruction if the key of value ``V[X]`` isn't pressed
        '''
        if self.key[self.V[self.x]] == 0:
            self.PC += 2

    def OP_Fxxx(self):
        '''OP_Fxxx
        :Version: 1.0.0
        
        Execute ``OP_F000`` instructions.
        
            **Known instructions**
                - ``FX33``: stores BCD representation of ``V[X]`` in memory from the adress ``I``
                - ``FX15``: sets the delay timer to the value of ``V[X]``
                - ``FX55``: copies the values from ``V[0]`` to ``V[X]`` (included) in the memory, starting from the adress ``I`` and ``I`` must be set after its last adress modified
                - ``FX65``: copies the values starting from the address ``I`` into ``V[0]`` to ``V[X]``, and sets ``I`` to after the latest value retrieved
                - ``FX07``: sets ``V[X]`` equal to delay timer
                - ``FX18``: sets sound timer value equal to ``V[X]``
                - ``FX29``: sets ``I`` equal to the location of the character sprite corresponding to the value of ``V[X]``
                - ``FX0A``: waits for a key press, and stores the pressed key in ``V[X]``
                - ``FX1E``: sets ``I = I + V[X]``
        '''
        result = self.currentOpcode & 0x000F
        if(result == 0x0003):   # FX33: stores BCD representation of V[X] in memory from the adress I
            # BCD representation is a way to easily get decimal digits from a binary number.
            # It consists in separating each digit of a decimal number and storing them independantly. Useful to store scores for instance.
            # Ex.: 231 will get you 2, 3 and 1.
            # Here we get those 3 digits (because V[X] is an 8 bits number which goes to 255) and store them in the memory at adress I for the first one, I+1 for the second and I+2 for the last one.
            # So : 	2 -> memory[I]
            #		3 -> memory[I+1]
            #		1 -> memory[I+2]
            self.memory[self.I] = self.V[self.x] // 100 													# // It is an euclidian division, without the rest. So by doing this with 231, we get 2 which is what we want
            self.memory[self.I+1] = (self.V[self.x] - (self.memory[self.I] * 100)) // 10 					# To get the second digit, we substract from the original number the hundreds we just got (2*100) so we have 231-200=31. We then do 31//10 and we get 3
            self.memory[self.I+2] = self.V[self.x] - self.memory[self.I]*100 - self.memory[self.I+1]*10 	# For the units, we just substract the hundreds and tens. 231 - (2*100) - (3*10) = 1

        elif(result == 0x0005): # FXx5
            
            result = self.currentOpcode & 0x00F0
            
            if(result == 0x0010):   # FX15: sets the delay timer to the value of V[X]
                self.delay_timer = self.V[self.x]

            elif(result == 0x0050): # FX55: copies the values from V[0] to V[X] (included) in the memory, starting from the adress I and I must be set after its last adress modified
                for i in range(0,self.x+1): 				# For each value from 0 to X included
                    self.memory[self.I+i] = self.V[i] 	# Sets V[i] into memory[I+i]
                self.I += self.x + 1 						# Sets I to I + X + 1 so we are the next cell than the last we modified

            elif(result == 0x0060): # FX65: copies the values starting from the adress I into V[0] to V[X], and sets I to after the latest value retrieved
                for i in range(0, self.x+1): 				# The for loop works the same as before, going from 0 to X included
                    self.V[i] = self.memory[self.I+i] 	# The assignation is just reversed
                self.I += self.x + 1 						# Sets I to I + X + 1

        elif(result == 0x0007): # FX07: V[X] = delay timer
            self.V[self.x] = self.delay_timer

        elif(result == 0x0008): # FX18: sound timer = V[X]
            self.sound_timer = self.V[self.x]

        elif(result == 0x0009): # FX29: sets I = location of the character sprite corresponding to the value of V[X]
            '''
            Because we put our sprites from the adress 0, we just have to do I = V[X]*5.
            If we want the sprite for 0, 0*5 = adress 0
            But if we want the sprite for 8, 8*5 = adress 40
            If we didn't put the sprites at 0x00, we would just have added to I the starting adress of the sprites. But by putting them at 0x00 we don't even have to do that
            '''
            self.I = self.V[self.x]*5

        elif(result == 0x000A): # FX0A: waits for a key press, and stores the pressed key in V[X]. 
            '''
            It stops everything, so we will use an infinite loop.
            The side effect is that all the Python script is locked until we press the key.
            I'm pretty sure we could avoid the infinite loop here and keeping the rest of the code by making the PC goes a step before at the end of the instruction if no key is pressed
            This way if no key is pressed, the script still listens to event (to quit, etc.) but the next instruction is still this one
            And if we press a key, we don't decrement the PC and all goes on
            '''
            isKeyPressed = False 						# Boolean to get out of the loop (redundant with the break)
            #while not isKeyPressed: 					# (In origine era decommentato ma in questo modo alcuni giochi non partivano) While no key is pressed we stay inside the loop
            print(self.key)#DEBUG
            for idx, val in enumerate(self.key): 	# For each available key
                if(val == 1): 						# If the current one is pressed
                    isKeyPressed = True 			# We set the boolean to True
                    self.V[self.x] = idx 			# Store the value of the key in V[X]
                    break 							# Break out of the loop

        elif(result == 0x000E): 	# FX1E: sets I = I + V[X]
            self.I += self.V[self.x]

    def executeOpcode(self):
        '''executeOpcode
        :Version: 1.0.0
        
        Executes the action linked to an opcode
        '''
        result = self.currentOpcode & 0xF000    # First mask to switch on opcodes with their first hexadecimal value, which differentiates a lot of them
        
        #print('Current opcode:',hex(opcode))#DEBUG
        
        # We will then use masking if needed to differentiate opcodes starting with the same hexadecimal value, and again if needed for the third and fourth.
        if(self.currentOpcode == 0x00E0):   # Clear the screen
            self.OP_00E0()
        elif(self.currentOpcode == 0x00EE): # Returns from a subroutine
            self.OP_00EE()
        elif(result == 0x1000): # 1NNN : jumps to the location NNN in the memory
            self.OP_1NNN()
        elif(result == 0x2000): # 2NNN : calls subroutine NNN. This is like a jump but we save the current PC to return at the end of the routine
            self.OP_2NNN()
        elif(result == 0x3000): # 3XKK : skip next instruction if V[X] and KK are equal
            self.OP_3XKK()
        elif(result == 0x4000): # 4XKK : skip next instruction if V[X] and KK are not equal
            self.OP_4XKK()
        elif(result == 0x5000): # 5xxx
            self.OP_5XY0()
        elif(result == 0x6000): # 6XKK : sets the value KK into V[X]
            self.OP_6XKK()
        elif(result == 0x7000): # 7XKK : adds KK to V[X]
            self.OP_7XKK()
        elif(result == 0x8000): # 8xxx
            self.OP_8xxx()
        elif(result == 0x9000): # 9xxx
            self.OP_9XY0()
        elif(result == 0xA000): # ANNN : sets I = NNN
            self.OP_ANNN()
        elif(result == 0xB000): # BNNN : jumps to location NNN + V[0]
            self.OP_BNNN()
        elif(result == 0xC000): # CXKK : sets V[X] = (random byte) AND KK
            self.OP_CXKK()
        elif(result == 0xD000): # DXYN
            self.DXYN(self.screen.pixels)
        elif(result == 0xE000): # EXxx
            self.OP_Exxx()
        elif(result == 0xF000): # FXxx
            self.OP_Fxxx()

        self.PC += 2 	# After we executed our instruction, we go to the next step with the PC, which is 2 memory cells later because we have 16 bits opcodes and a cell is only 8 bits

    def _bitLen(self,value):
        '''_bitLen
        :Version: 1.0.0
        
        Gives the length of an unsigned value in bits
        
        :param value:
        :type value: int
        
        :return: the length of an unsigned value in bits
        :rtype: int
        '''
        length = 0
        while (value):
            value >>= 1
            length += 1
        return(length)

    def _getMSB(self,value,size):
        '''_getMSB
        :Version: 1.0.0
        
        Gets the most significant bit of an unsigned value in a size-bit format
        
        :param value:
        :type value: int
        :param size: The max length of our binary value
        :type size: int
        
        :return: MSB of a value
        :rtype: int
        '''
        return 1 if (self._bitLen(value) == size) else 0

    def _getLSB(self,value):
        '''_getLSB
        :Version: 1.0.0
        
        Gets the least significant bit of a value
        
        :param value:
        :type value: int
        
        :return: LSB of a value
        :rtype: int
        '''
        return (value & 0x1)



if __name__ == '__main__':
    pass
