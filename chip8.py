'''

cippotto is a(nother) CHIP-8 Emulator written in Python by netcat n4d4s

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

CopyLeft 2022

'''

import pygame
from pygame.locals import *
from os.path import basename as osBasename

from emuSettings import Configurations

from chip8CPU import CPU
from chip8Display import Display
from chip8Inputs import Keymap
from audio import AudioSynthesizer

class CHIP8(CPU,Keymap,Configurations):
    '''
    CHIP-8 emulator, this load the ROM, decode opcodes and execute them.
    
    **This is like a BUS**
    
    .. todo::
        - Modify this class, ``chip8Display`` and ``audio`` to let that the BUS inherit from ``chip8Display`` and ``audio`` as is already done with ``CPU`` ``Keymap`` and ``Configurations``
    '''

    def __init__(self):
        '''
        CHIP8 __init__
            Init CHIP8 class
            
        ''' 
        Configurations.__init__(self)                                   # Getting emulator and software configurations
        CPU.__init__(self)                                              # Connect CPU to the BUS
        Keymap.__init__(self)                                           # Connect inputs logics to BUS
        self.screen = Display()                                         # Initialize screen stuff
        self.screen.setSoftwareColor(self.bgColor,self.fgColor,self.debugSquareColor)   # Set game screen and debug area colors
        self.screen.setSoftwareIcon(self.emulatorIcon)                  # Software icon used by PyGame during the game
        self.screen.screenTitle = self.screenTitle
        self.screen.preInitSound(self.audioFreq, self.audioChannelsNum) # To set the number of audiochannels we must pre init audio here, before PyGame init
        self.screen.pygameInit()                                        # Initialize PyGame and set something
        self.audio = AudioSynthesizer()                                 # Initialize audio stuff
        self.audio.setDebugMode(self.debugAudioOnCLI)
        
        self.romPath = ''       # Path to the ROM file
        self.changeRom = False  # Boolean to put to True when the user wants to change the ROM. It allows the program to not end when unloading the current ROM

        self.started = False    # Boolean to check if the game is launched
        self.paused = False     # Boolean to pause the emulator
        self.nextStep = False   # Boolean to allow the emulator to jump to the next step, try when game is paused

        self.DELAYSOUNDTIMER = USEREVENT + 1    # Pygame event for the 60Hz timers and the screen refreshing

        self.sound = True       # Boolean to allow sound to be played
        
        self.setKeymap(self.keyboard)   # Set keymap with given option value


    def setRomPath(self,romPath=''):
        '''setRomPath
        :Version: 1.0.0
        
        Set ``self.romPath``
        
        :param romPath: The path of the ROM
        :type romPath: string 
        '''
        self.romPath = romPath

    def loadGame(self):
        '''loadGame
        :Version: 1.0.0
        
        Loads the game from the ROM file into memory
        '''
        with open(self.romPath, 'rb') as f:                     # Opening the ROM
            while 1:                                            # While the universe is working
                byte = f.read(1)                                # We get a byte
                if not byte:                                    # If it is empty we reached the EOF
                    if self.debugCLI:
                        print('cippotto BUS ... ROM fully loaded in memory') #DEBUG
                        print('cippotto BUS ... Loaded ' + self.romPath) #DEBUG
                    break
                if self.debugCLI:
                    print('cippotto BUS ... Loading ROM to memory {}'.format(hex(self.startMemory)))#DEBUG
                self.memory[self.startMemory] = int.from_bytes(byte, 'big', signed=False)     # Storing the value of the byte we read into the memory at the startMemory position
                self.startMemory += 1                                                         # Increasing the counter

    def startGame(self):
        '''startGame
        :Version: 1.0.0
        
        Start the game, listen events, get opcodes and execute them
        
        :return: False will quit entire emulator, True will let the user to select another ROM, see ``main.py`` to find how this is managed
        :rtype: bool
        '''
        self.screen.setScreenTitle(osBasename(self.romPath))    # Modify screen title adding the game name
        self.screen.setDebugMargin(self.debugOnWindow)
        if self.debugCLI:
            print('cippotto BUS ... self.audioFreq = ',self.audioFreq)#DEBUG
        self.audio.setFreq(self.audioFreq)
        self.audio.setSampleRate(self.audioSampleRate)
        self.screen.startScreen()     # Start the screen
        self.audio.initMixer()
        pygame.time.set_timer(self.DELAYSOUNDTIMER, self.delayInterval) # Sets up the 60Hz timer
        if self.debugCLI:
            print('cippotto BUS ... self.delayInterval = ',self.delayInterval)#DEBUG
        
        self.started = True     # Sets the started boolean to True

        while self.started:     # While the emulator is started

            self.listen()           # Checks all Pygame events on queue (timers, window resizing, quitting the program or most of all pressing keys)
            self.getCurrentOpcode() # Retrieve current opcode and store it in CPU
            self.executeOpcode()    # Execute the current opcode

            while self.paused and self.started: # If the game is on pause
                self.nextStep = False           # We reset the boolean value
                self.listen()                   # We listen to the events (to not block the program if we want to quit or anything) but we are mainly waiting for the next step key to be pressed
                if self.nextStep:               # If the next step key was pressed
                    break                       # We break out of the paused loop so we can execute the next opcode. But as the game is paused, if will immediatly go back into this loop right after the next opcode, to wait for the next step
                pygame.time.wait(50)           # Delay of 50ms, to avoid using too much useless CPU time (I guess we don't really care but hey 20Hz to wait for a key event for the next step or quitting is nice enough)

            #self.screen.clock.tick(self.screen.fps) # This is a test to slow the games
            pygame.time.wait(round((1/self.speed)*1000))     # Delay allowing us to execute opcodes at the specified speed

        self.screen.destroyScreen()     # When out of the loop, we are quitting so we destroy the screen
        return self.changeRom             # Returning to the main program whether to quit or not to quit

    def rebootGame(self):
        '''rebootGame
        :Version: 1.0.0
        
        Reboot the game
        '''
        self.initVars()     # Initialize variables
        self.screen.clear() # Clear the screen
        if self.debugCLI:
            print('cippotto BUS ... self.audioFreq = ',self.audioFreq)#DEBUG
        self.audio.setFreq(self.audioFreq)
        self.audio.setSampleRate(self.audioSampleRate)
        self.loadGame()     # Reload the game into memory

    def listen(self):     
        '''listen
        :Version: 1.0.0
        
        Goes through all Pygame events on queue
        '''
        for event in pygame.event.get():    # For each event in the queue
            if event.type == QUIT:          # QUIT
                self.started = False        # We stop the game
            elif event.type == self.DELAYSOUNDTIMER:    # 60Hz timer
                self.timeout60Hz()                      # Function refreshing screen and managing Chip 8 timers
            elif event.type == VIDEORESIZE:                 # Resizing the screen
                if self.debugCLI:
                    print('cippotto BUS ... event.w =',event.w,', event.w =',event.h)#DEBUG
                self.screen.resizeScreen(event.w, event.h)     # Function to resize the screen and reset it
                self.screen.togglePause(self.paused)        # Redraw PAUSE label if necessary
                self.screen.toggleSound(self.sound)         # Redraw MUTE label if necessary
            elif event.type == KEYDOWN:     # Pressing a key
                try:
                    self.key[self.keyMap.index(event.key)] = 1
                except ValueError:  # ValueError to intercept MENU KEYS
                    if event.key == K_ESCAPE:
                        self.started = False
                    elif event.key == K_F1:
                        self.started = False
                        self.changeRom = True
                    elif event.key == K_F2:
                        self.rebootGame()
                    elif event.key == K_F3:
                        self.paused = not self.paused
                        self.screen.togglePause(self.paused)
                    elif event.key == K_F4:
                        self.nextStep = True
                    elif event.key == K_F5:
                        self.sound = not self.sound
                        self.screen.toggleSound(self.sound)
                    elif event.key == K_F6:
                        self.debugOnWindow = not self.debugOnWindow
                        self.screen.setDebugMargin(self.debugOnWindow)
                        self.screen.togglePause(self.paused)
                        self.screen.toggleSound(self.sound)
            elif event.type == KEYUP:
                try:
                    self.key[self.keyMap.index(event.key)] = 0
                except ValueError:
                    pass
            if self.debugCLI:        
                print('cippotto BUS ... self.key = ' + str(self.key))#DEBUG

    def timeout60Hz(self):
        '''timeout60Hz
        :Version: 1.0.0
        
        To be called at a frequency of 60Hz.
        Decrements the emulator timers and refreshes the screen
        '''
        if(self.delay_timer > 0):     # We decrement the delay timer if it isn't 0
            self.delay_timer -= 1

        if(self.sound_timer > 0):     # Same for the sound timer
            self.sound_timer -= 1
            if self.sound:     # And if it isn't 0 and we are allowing the sound to be played, we play a beep
                #self.screen.beep.play()    # With this you will use a a file to play sound
                if self.sound_timer != 0:
                    duration = int(self.timerHZ/(self.sound_timer))
                    if self.debugCLI:
                        print('cippotto BUS ... Sound duration:',duration)#DEBUG
                    self.audio.playAudio(self.audio.squareWave(self.audioChannelsNum), duration)
            #self.sound_timer -= 1

        if self.started:                                                                                             # If the game is started
            if self.debugOnWindow:
                if self.debugCLI:
                    print('cippotto BUS ... Printing debug in PyGame window')#DEBUG
                self.screen.displayRegsAndVars(self.V, self.delay_timer, self.sound_timer, self.I, self.PC, self.SP)     # We update the debugger values with our registers
                self.screen.displayMemory(self.memory)                                                                     # Same with our memory
            if self.debugCHIP8StatusOnFile:
                self._dumpMemoryAndReg()
                self._dumpPixels()
            self.screen.refreshScreen()                                                                             # We refresh the screen

    def _dumpMemoryAndReg(self):
        '''_dumpMemoryAndReg
        :Version: 1.0.0
        
        Dumps the whole memory and register values to a file
        
        Maybe using this method cippotto will slow
        '''
        result = "Opcode: {}\nPC: {}\nI: {}\nDT: {}\nST: {}\nSP: {}\nLast Stack: {}\n\n---------------------------------------------\n\n".format(hex(self.getCurrentOpcode()), hex(self.PC), hex(self.I), hex(self.delay_timer), hex(self.sound_timer), hex(self.SP), hex(self.stack[self.SP-1]))
        for idx, value in enumerate(self.V):
            result += "V[{}] = {} = {}\n".format(idx, hex(value), value)
        result += "\n\n---------------------------------------------\n\n"
        result += str(self.memory)
        with open(self.memoryDumpFile, 'w') as f:
            f.write(result)

    def _dumpPixels(self):
        '''_dumpPixels
        :Version: 1.0.0
        
        Dumps the status of all pixels in a file
        
        Maybe using this method cippotto will slow
        '''
        result = str(self.screen)
        with open(self.pixelsDumpFile, 'w') as f:
            f.write(result)


if __name__ == '__main__':
    pass
