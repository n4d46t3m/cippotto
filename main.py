'''

cippotto is a(nother) CHIP-8 Emulator written in Python by netcat n4d4s

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

CopyLeft 2022

'''

if __name__ == '__main__':

    from chip8 import CHIP8                 # Import cippotto emulator
    from emuSettings import Configurations  # Import cippotto configurations
    from selectRom import SelectRom         # Import cippotto welcome screen and ROM selector

    conf = Configurations()                 # Init configurations

    changeRom = True                        # Boolean to do an infinite loop until we don't want to change a ROM anymore (we quit)
    while changeRom:                        # While we are still wanting to play when quitting the loaded ROM
        selectRom = SelectRom(conf)         # Open welcome screen where the user can select the ROM. Current loop wait until the user select a ROM
        chip8 = CHIP8()                     # Only after selected a ROM, init Chip-8 BUS, this will load everything (Config, CPU, Keys, Screen, Audio) in the same place
        chip8.setRomPath(selectRom.romPath) # Setting ROM path
        chip8.loadGame()                    # Load the game into memory
        changeRom = chip8.startGame()       # Start the game and save the returned value when it ends. Current loop wait until the user want change ROM or quit cippotto 
        del chip8                           # We delete the Chip-8 object to restart with a new ROM if needed
