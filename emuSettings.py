'''

cippotto configurations written by netcat n4d4s

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

CopyLeft 2022

'''

class Configurations():
    '''
    Class used to create variables used by **cippotto**
    
    .. todo::
        - Get some of these variables reading a config file and/or command line options
        
    '''
    
    def __init__(self):
        '''
        Configurations __init__
            Init Configurations class
        '''
        self.rootRomsDir='ROMS/'                # The root dir to start to browse to select the ROM, see main.py
        self.memoryDumpFile='Logs/memdump'      # Where will be saved the memory dump file, see chip8.py
        self.pixelsDumpFile='Logs/pixelsdump'   # Where will be saved the pixel dump file, see chip8.py
        self.emulatorIcon='Assets/chip8_64x64.png'
        self.keyboard='QWERTY'                  # QWERTY or AZERTY, see chip8Inputs.py
        self.audioFreq=4400                      # see audio.py
        self.audioSampleRate=44100              # see audio.py can be 22050 44100
        self.audioChannelsNum=1                 # Number of channels, 1 for MONO, 2 for stereo. CHIP-8 original audio was mono
        
        self.debugOnWindow = False                 # Print registry and memory status in PyGame window
        self.debugCLI = False                   # Print some debug messages on stdout
        self.debugCHIP8StatusOnFile = False     # Print CHIP-8 status on files, set it to True only for hard debug or tests. This is an insane destructive debug method
        self.debugAudioOnCLI = False            # Print audio debug on stdout
        
        self.screenTitle='Cippotto - CHIP-8 Emulator by n4d46t3m'    # Screen title
        self.bgColor='#000000'          # Starting window and game background color (CHIP-8 original background color is #000000)
        self.fgColor='#00C306'          # Starting window and game foreground color (CHIP-8 original foreground color is #ffffff)
        self.debugSquareColor='#FFFF00' # Square color that refer to current opcode when debug is on during gameplay
    


if __name__ == '__main__':
    pass
    
