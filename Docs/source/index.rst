.. CIPPOTTO documentation master file, created by
   sphinx-quickstart on Sat Jul 30 16:58:15 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. mdinclude:: ../../README.md

Documentation indicies
======================

* :ref:`genindex`
* :ref:`modindex`

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

