chip8 module
============

.. automodule:: chip8
    :members:
    :undoc-members:
    :show-inheritance:
