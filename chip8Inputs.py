'''

CHIP-8 Keymap written by netcat n4d4s

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

CopyLeft 2022

'''

from pygame.locals import *

class Keymap:
    '''Keymap
    :Version: 1.0.0
    
    Chip-8 implementation used a 4x4 keypad with 16 keys labeled with HEX form ``0x0`` to ``0xF``
    
    Original key positions on keypad was this:
    
    .. code-block::
    
        1   2   3   C
        4   5   6   D
        7   8   9   E
        A   0   B   F
        
    So the HEX values were:
    
    .. code-block::
            
        0x1   0x2   0x3   0xC
        0x4   0x5   0x6   0xD
        0x7   0x8   0x9   0xE
        0xA   0x0   0xB   0xF
    
    In methods ``_qwertyMap()`` and ``_azertyMap()`` you can see the default mappings
    that this emulator will use
    '''

    def __init__(self):
        '''
        Keymap __init__
            Init Keymap class
            
        Initialize useful stuff to manage Chip-8 keyboard
        '''
        self.keyMap = None  # This will be a tuple that will contain selected keymap
        self.key = [0 for i in range(16)]   # Keys status, 0 isn't pressed, 1 is pressed
        #print('self.key = ' + str(self.key))#DEBUG
        
    def setKeymap(self, keymap='QWERTY'):
        '''setKeymap
        :Version: 1.0.0
        
        Given an option will set ``self.keyMap``, defaults to **QWERTY**
        
        :param keymap: can be ``QWERTY`` or ``AZERTY``
        :type keymap: string 
        '''
        if keymap == 'QWERTY':
            self.keyMap = self._qwertyMap()
        elif keymap == 'AZERTY':
            self.keyMap = self._azertyMap()
        else:
            self.keyMap = self._qwertyMap()
    
    def _qwertyMap(self):
        '''_qwertyMap
        :Version: 1.0.0
        
        On modern **QWERTY** keyboards we can map Chip-8 keys in this way:
        
        .. code-block::
        
            1   2   3   4
            Q   W   E   R
            A   S   D   F
            Z   X   C   V
            
        :return: keymap to be used with QWERTY keyboards
        :rtype: tuple
        '''
        return (
                    K_x,    # Chip-8 key 0x0
                    K_1,    # Chip-8 key 0x1
                    K_2,    # Chip-8 key 0x2
                    K_3,    # Chip-8 key 0x3
                    K_q,    # Chip-8 key 0x4
                    K_w,    # Chip-8 key 0x5
                    K_e,    # Chip-8 key 0x6
                    K_a,    # Chip-8 key 0x7
                    K_s,    # Chip-8 key 0x8
                    K_d,    # Chip-8 key 0x9
                    K_z,    # Chip-8 key 0xA
                    K_c,    # Chip-8 key 0xB
                    K_4,    # Chip-8 key 0xC
                    K_r,    # Chip-8 key 0xD
                    K_f,    # Chip-8 key 0xE
                    K_v     # Chip-8 key 0xF
                )   # Chip-8 keys
                
    def _azertyMap(self):
        '''_azertyMap
        :Version: 1.0.0
        
        On modern **AZERTY** keyboards we can map Chip-8 keys in this way:
        
        .. code-block::
        
            1   2   3   4
            A   Z   E   R
            Q   S   D   F
            W   X   C   V
            
        :return: keymap to be used with AZERTY keyboards
        :rtype: tuple
        '''
        return (
                    K_x,    # Chip-8 key 0x0
                    K_1,    # Chip-8 key 0x1
                    K_2,    # Chip-8 key 0x2
                    K_3,    # Chip-8 key 0x3
                    K_a,    # Chip-8 key 0x4
                    K_z,    # Chip-8 key 0x5
                    K_e,    # Chip-8 key 0x6
                    K_q,    # Chip-8 key 0x7
                    K_s,    # Chip-8 key 0x8
                    K_d,    # Chip-8 key 0x9
                    K_w,    # Chip-8 key 0xA
                    K_c,    # Chip-8 key 0xB
                    K_4,    # Chip-8 key 0xC
                    K_r,    # Chip-8 key 0xD
                    K_f,    # Chip-8 key 0xE
                    K_v     # Chip-8 key 0xF
                )   # Chip-8 keys



if __name__ == '__main__':
    pass
