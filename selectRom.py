'''

cippotto welcome screen and file selector written by netcat n4d4s

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

'''

from tkinter import Tk
from tkinter.filedialog import askopenfilename
from tkinter import PhotoImage as tkPhotoImage
from tkinter import Text
from tkinter import Label
from tkinter import Button

class SelectRom:
    '''
    This class only provide a file selector and some funny stuff for user welcome
    It isn't wired with **cippotto** BUS
    
    This shit is based on **Tkinter**, maybe one day I will integrate this file selector in **PyGame** interface
    
    .. todo::
        - In the welcome screen let the user to use F5 and F6 buttons to set sound and debug before the game start and display current values
        - Create an option page reachable through a button, in this option page the user will can change something like game and menù colors
    
    '''
    
    def __init__(self,configurations):
        '''
        SelectRom __init__
            Init SelectRom class
        '''
        self.windowFgColor=configurations.fgColor
        self.windowBgColor=configurations.bgColor
        self.root = Tk()
        self.root.geometry('320x320')
        self.root.configure(background=self.windowBgColor)
        self._setIcon(configurations.emulatorIcon)
        self._setRomsDirectory(configurations.rootRomsDir)
        self.root.title(configurations.screenTitle)
        self.root.protocol('WM_DELETE_WINDOW', self.closeSoftware) # When Tkinter welcome window is closed
        self._writeWelcome()
        self.root.mainloop()
        
    def _setIcon(self,emulatorIcon=''):
        '''_setIcon
        :Version: 1.0.0
        
        Set the welcome window icon
        
        :param emulatorIcon: the path of cippotto icon
        :type emulatorIcon: string 
        '''
        self.root.iconphoto(False, tkPhotoImage(file=emulatorIcon))
        
    def _setRomsDirectory(self,rootRomsDir=''):
        '''_setRomsDirectory
        :Version: 1.0.0
        
        Set the welcome window icon
        
        :param rootRomsDir: Directory where ROMS are located, the file selector will open in this location
        :type rootRomsDir: string 
        '''
        self.rootRomsDir = rootRomsDir
        
    def _writeWelcome(self):
        '''_writeWelcome
        :Version: 1.0.0
        
        Print in welcome screen some useful things
        '''
        tkTitle = Label(self.root,text='Cippotto useful keys',bg=self.windowBgColor,fg=self.windowFgColor,font='-size 20 -weight bold')
        tkSubTitle = Label(self.root,text='You can use these keys when a ROM is loaded',bg=self.windowFgColor,fg=self.windowBgColor,pady=15,font='-size 9  -weight bold')
        tkSubTitleMargin = Label(self.root,text=' ',bg=self.windowBgColor,fg=self.windowFgColor)
        chip8ESC = Label(self.root,text='ESC: Quit',bg=self.windowBgColor,fg=self.windowFgColor)
        chip8ChangeRom = Label(self.root,text='F1: Change ROM',bg=self.windowBgColor,fg=self.windowFgColor)
        chip8ResetRom = Label(self.root,text='F2: Reboot ROM',bg=self.windowBgColor,fg=self.windowFgColor)
        chip8Pause = Label(self.root,text='F3: Pause / Unpause',bg=self.windowBgColor,fg=self.windowFgColor)
        chip8NextStep = Label(self.root,text='F4: Next step',bg=self.windowBgColor,fg=self.windowFgColor)
        chip8ToggleSound = Label(self.root,text='F5: Sound ON / OFF',bg=self.windowBgColor,fg=self.windowFgColor)
        chip8ToggleDebug = Label(self.root,text='F6: Debug ON / OFF',bg=self.windowBgColor,fg=self.windowFgColor)
        margin = Label(self.root,text=' ',bg=self.windowBgColor,fg=self.windowFgColor,pady=1)
        selectRomBtn = Button(self.root,text='SELECT ROM',bg=self.windowBgColor,fg=self.windowFgColor,activebackground=self.windowFgColor,activeforeground=self.windowBgColor,command=self.openFileSelector)
        tkTitle.pack()
        tkSubTitle.pack()
        tkSubTitleMargin.pack()
        chip8ESC.pack()
        chip8ChangeRom.pack()
        chip8ResetRom.pack()
        chip8Pause.pack()
        chip8NextStep.pack()
        chip8ToggleSound.pack()
        chip8ToggleDebug.pack()
        margin.pack()
        selectRomBtn.pack()
        
    def openFileSelector(self):
        '''openFileSelector
        :Version: 1.0.0
        
        Open a file selector at ``self.rootRomsDir`` to permit the user to choice a ROM
        This will modify ``self.romPath`` that will contain the path of selected ROM
        Depending on user action, this method can close rom selector and welcome screen or entire software
        
        '''
        self.romPath = askopenfilename(initialdir=self.rootRomsDir)
        if self.romPath == '' or not isinstance(self.romPath, str):
            self.closeSoftware()
        else:
            self.selectorDie()
        
    def selectorDie(self):
        '''selectorDie
        :Version: 1.0.0
        
        Close welcome screen destroying Tkinter
        '''
        self.root.destroy()          # Destroying Tkinter
        
    def closeSoftware(self):
        '''closeSoftware
        :Version: 1.0.0
        
        Call ``self.selectorDie`` to kill Tkinter, then exit program
        '''
        self.selectorDie()
        exit()




if __name__ == '__main__':
    pass
