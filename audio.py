'''

cippotto audio system written by netcat n4d4s

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

CopyLeft 2022

'''

import pygame, pygame.sndarray
import numpy as np
import scipy.signal as scipySignal
        
class AudioSynthesizer:

    def __init__(self):
        '''
        AudioSynthesizer __init__
            Init AudioSynthesizer class
        '''    
        self.frequency = 0  # Frequency in Hz (This value is set by method setFreq)
        self.rate = 0       # Sample rate (This value is set by method setSampleRate)
        self.peak = 20      # Max audio volume, 127 = volume 100%

    def setDebugMode(self, debugMode=False):
        '''setDebugMode
        :Version: 1.0.0
        
        Turn on/off the audio debug on CLI setting ``self.debug``
        
        :param debugMode: ``True`` to set debug on, otherwise ``False``
        :type debugMode: boolean 
        '''
        self.debug = debugMode

    def setFreq(self, frequency=440):
        '''setFreq
        :Version: 1.0.0
        
        Given a frequency set ``self.frequency``
        The frequency is the note produced by CHIP-8 beep
        Given a frequency of 440 the theory tell that the note that will played is 'A'
        
        :param frequency: Note
        :type frequency: int 
        '''
        self.frequency = frequency
        if self.debug:
            print('Audio DEBUG - Setting audio frequency to',self.frequency)#DEBUG
        
    def setSampleRate(self, rate=44100):
        '''setSampleRate
        :Version: 1.0.0
        
        Given a sample rate set ``self.rate``
        
        :param rate: Sample rate
        :type rate: int 
        '''
        self.rate = rate
        if self.debug:
            print('Audio DEBUG - Setting audio sample rate to',self.rate)#DEBUG

    def initMixer(self):
        '''initMixer
        :Version: 1.0.0
        
        Init PyGame audio
        '''
        if self.debug:
            print('Audio DEBUG - Init mixer')#DEBUG
        #pygame.mixer.pre_init(frequency=self.rate, channels=self.channels, allowedchanges=0)   # This must go before pygame.init()
        pygame.mixer.init()

    def playAudio(self, sampleWave, ms):
        '''playAudio
        :Version: 1.0.0
        
        Play the given NumPy array, as a sound, for ms milliseconds.
        
        :param sampleWave: The array that should bu played as a sound
        :type sampleWave: NumPy Array, can be bidimensional for stereo sound
        :param ms: Sound duration in milliseconds
        :type ms: int 
        '''
        if self.debug:
            print('Audio debug - sound duration = ',ms)#DEBUG
        sound = pygame.sndarray.make_sound(sampleWave)
        sound.play(0,ms)

    def squareWave(self, channels=1):
        '''squareWave
        :Version: 1.0.0
        
        Compute N samples of a square wave using frequency and peak amplitude.
        
        :param channels: 1 for mono, 2 for stereo
        :type channels: int 
        
        :return: Square Wave
        :rtype: NumPy Array
        '''
        if self.debug:
            print('Audio debug - self.frequency = ',self.frequency)#DEBUG
        t = np.linspace(0, 1, self.rate, endpoint=False)
        wave = scipySignal.square(2 * np.pi * self.frequency * t)
        wave = np.resize(wave, self.frequency)
        audioBuffer = self.peak * wave.astype(np.int8)
        if channels == 2:
            if self.debug:
                print(np.repeat(audioBuffer.reshape(self.rate, 1), 2, axis = 1))#DEBUG
            return(np.repeat(audioBuffer.reshape(self.rate, 1), 2, axis = 1))
        if channels == 1:
            if self.debug:
                print(audioBuffer)#DEBUG
            return audioBuffer
        
if __name__ == '__main__':
    audio = AudioSynthesizer()
    audio.setFreq(100)
    audio.setSampleRate(44100)
    '''
    pygame.init()                                   # Useless, only for testing purposes here
    window = pygame.display.set_mode((300, 300))    # Useless, only for testing purposes here
    '''
    audio.initMixer()
    audio.playAudio(audio.squareWave(), 15)
    

